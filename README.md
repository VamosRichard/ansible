# Ansible



## Inventory

Az inventory.ini fájlban a konfigurálni kivánt szerverek/kliensek szerepelnek, amiket IP-cím alapján azonosítunk.

## Samba role

A Samba role feltelepíti a Sambát és minden hozzá tartozó függőséget.

Ezután elindítja, és engedélyezi a tűzfal beállításoknál a samba service-t, illetve az SELinux kontextust is rögzíti.

Létrehozza a samba share-hez köthető könyvtárat, illetve az /etc/samba/smb.conf fájlba bemásolja a paramétereket.

A legvégén egy handler segítségével újraindítja a Sambát.

## Samba.mount role

Telepíti a cifs csomagot.

Létrehozza a mountpontként szolgáló könyvtárat.

Felcsatolja az fstab-ba a samba share-nél megadott könyvtárat a mountpontba (a feladatkiírásban az szerepelt, hogy UUID alapján kellett volna, de egyeztettünk erről, hogy arra nem tudunk lehetséges megoldást)


## Cr(eate).directory role

Létrehoz egy könyvtárat tulajdonossal és jogosultsággal.

## Playbook.yml

A playbook két egymástól független playből áll: az első a Sambával kapcsolatos telepítést/konfigurálást foglalja magában egy adott szerveren, a második pedig a mountolással/fstabbal kapcsolatos beállításokat kezeli, illetve egy funkcionálisan különálló könyvtárat is létrehoz egy hoston.

